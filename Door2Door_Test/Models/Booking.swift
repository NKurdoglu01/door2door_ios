//
//  Message.swift
//  Door2Door_Test
//
//  Created by Naci Kurdoglu on 25/2/21.
//

import Foundation
import SwiftyJSON

class Booking {
        
    var status:Status
    
    var vehicleLocation:Location
    let pickuplocation:Location
    let dropoffLocation:Location
    var intermediateStopLocations:[Location] = []
    
    init(json:JSON){
                
        let tempStatus = json["status"].stringValue
        status =  Status(rawValue: tempStatus)!
        
        let tempVehicleLoc = json["vehicleLocation"]
        vehicleLocation = Location(lat: tempVehicleLoc["lat"].doubleValue, long: tempVehicleLoc["lng"].doubleValue, address: tempVehicleLoc["address"].string)
        
        let tempPickupLocation = json["pickupLocation"]
        pickuplocation = Location(lat: tempPickupLocation["lat"].doubleValue, long: tempPickupLocation["lng"].doubleValue, address: tempPickupLocation["address"].string)
        
        let tempDropoffLocation = json["dropoffLocation"]
        dropoffLocation = Location(lat: tempDropoffLocation["lat"].doubleValue, long: tempDropoffLocation["lng"].doubleValue, address: tempDropoffLocation["address"].string)
        
        let tempStopLocs = json["intermediateStopLocations"].arrayValue
        
        for tempStopLoc in tempStopLocs {
            
            let intermediateStopLoc = Location(lat: tempStopLoc["lat"].doubleValue, long: tempStopLoc["lng"].doubleValue, address: tempStopLoc["address"].string)
            intermediateStopLocations.append(intermediateStopLoc)
        }
    }
    
    func updateStatus(json:JSON){
        status =  Status(rawValue: json["data"].stringValue)!
    }
    
    func updateVehicleLocation(json:JSON){
        vehicleLocation = Location(lat: json["lat"].doubleValue, long: json["lng"].doubleValue, address: json["address"].string)
    }
    
    func updateIntermediateStops(json:[JSON]){
        
        var newStopLocations:[Location] = []
        for tempStopLoc in json {
            
            let intermediateStopLoc = Location(lat: tempStopLoc["lat"].doubleValue, long: tempStopLoc["lng"].doubleValue, address: tempStopLoc["address"].string)
            newStopLocations.append(intermediateStopLoc)
        }
        
        self.intermediateStopLocations = newStopLocations
    }
    
}
