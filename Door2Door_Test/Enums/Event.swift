//
//  Event.swift
//  Door2Door_Test
//
//  Created by Naci Kurdoglu on 25/2/21.
//

import Foundation

enum Event:String {
    
    case bookingOpened = "bookingOpened"
    case vehicleLocationUpdated = "vehicleLocationUpdated"
    case statusUpdated = "statusUpdated"
    case intermediateStopLocationsChanged = "intermediateStopLocationsChanged"
    case bookingClosed = "bookingClosed"
    case invalid
}
