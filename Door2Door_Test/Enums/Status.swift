//
//  Status.swift
//  Door2Door_Test
//
//  Created by Naci Kurdoglu on 25/2/21.
//

import Foundation

enum Status:String{
    
    case waitingForPickup = "waitingForPickup"
    case inVehicle = "inVehicle"
    case droppedOff = "droppedOff"
    case invalid
}
