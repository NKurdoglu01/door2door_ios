//
//  Location.swift
//  Door2Door_Test
//
//  Created by Naci Kurdoglu on 25/2/21.
//

import Foundation

struct Location{
    let lat:Double
    let long:Double
    let address:String?
}
