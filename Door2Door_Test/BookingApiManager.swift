//
//  ApiManager.swift
//  Door2Door_Test
//
//  Created by Naci Kurdoglu on 25/2/21.
//

import Foundation

class BookingApiManager {
    
    private static let urlString = "wss://d2d-frontend-code-challenge.herokuapp.com."
    private var webSocketTask:URLSessionWebSocketTask!
    
    init(onSuccess: @escaping(String)->(), onError: @escaping(String)->()) {
        webSocketTask = URLSession(configuration: .default).webSocketTask(with: URL(string: BookingApiManager.urlString)!)
        webSocketTask.resume()
        
        self.receiveMessage(onSuccess: onSuccess, onError: onError)
    }
    
    func stopConnection(){
        self.webSocketTask.cancel()
    }
    
    private func receiveMessage(onSuccess: @escaping(String)->(), onError: @escaping(String)->()) {
        webSocketTask.receive { [weak self] (result) in
            switch result {
              case .failure(let error):
                
                onError(error.localizedDescription)
                print("Error in receiving message: \(error)")
                
              case .success(let message):
                
                switch message {
                case .string(let text):
                    self?.receiveMessage(onSuccess: onSuccess, onError: onError)
                    onSuccess(text)
                case .data:
                  onError("Unknown format")
                @unknown default:
                    onError("Unknown format")
                }
            }
        }
    }
    
}
