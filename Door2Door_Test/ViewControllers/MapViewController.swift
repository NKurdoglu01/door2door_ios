//
//  ViewController.swift
//  Door2Door_Test
//
//  Created by Naci Kurdoglu on 25/2/21.
//

import UIKit
import MapKit
import SwiftyJSON

class MapViewController: UIViewController {
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var pickupLabel: UILabel!
    @IBOutlet weak var dropoffLabel: UILabel!
    private var booking:Booking?
    
    private var vehicleLocation:MKAnnotation = MKPointAnnotation()
    private var intermediateStops:[MKAnnotation] = []
    
    var manager:BookingApiManager!
    
    deinit {
        print("Deinit MapViewController called")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.manager = BookingApiManager(onSuccess: { [weak self] (message) in
            DispatchQueue.main.async {
                let json = JSON.init(parseJSON:message)
                
                let event = Event(rawValue: json["event"].stringValue)

                switch event{
                case .bookingOpened:
                    self?.booking = Booking(json: json["data"])
                    
                    self?.setPickupLocation()
                    self?.setDropoffLocation()
                    self?.updateVehicleLocation()
                    self?.updateIntermediateStops()
                    self?.updateStatus()
                
                case.vehicleLocationUpdated:
                    self?.booking?.updateVehicleLocation(json: json["data"])
                    self?.updateVehicleLocation()
                    
                case.statusUpdated:
                    self?.booking?.updateStatus(json: json)
                    self?.updateStatus()

                case.intermediateStopLocationsChanged:
                    self?.booking?.updateIntermediateStops(json: json["data"].arrayValue)
                    self?.updateIntermediateStops()
                    
                case.bookingClosed:
                    self?.bookingClosed()
                    
                default:
                    break
                }
            }
        }, onError: { (error) in
            print(error)
        })
        
    }
    
    func setPickupLocation(){
                
        if let booking = booking {
            let coords = CLLocationCoordinate2D(latitude: CLLocationDegrees(booking.pickuplocation.lat)  , longitude: CLLocationDegrees(booking.pickuplocation.long))
            let region = MKCoordinateRegion(center: coords, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05))

            self.mapView.setRegion(region, animated: true)

            let annot = MKPointAnnotation()
            annot.coordinate = coords
            annot.title = "Pickup"
            mapView.addAnnotation(annot)
            
            self.pickupLabel.text = booking.pickuplocation.address

        }
    }

    func setDropoffLocation(){
        
        if let booking = booking {
            let coords = CLLocationCoordinate2D(latitude: CLLocationDegrees(booking.dropoffLocation.lat)  , longitude: CLLocationDegrees(booking.dropoffLocation.long))
            
            let annot = MKPointAnnotation()
            annot.coordinate = coords
            annot.title = "Dropoff"
            mapView.addAnnotation(annot)
            
            self.dropoffLabel.text = booking.dropoffLocation.address
        }
        
    }
    
    func updateVehicleLocation(){
        
        self.mapView.removeAnnotation(self.vehicleLocation)
        if let booking = booking {
            let coords = CLLocationCoordinate2D(latitude: CLLocationDegrees(booking.vehicleLocation.lat)  , longitude: CLLocationDegrees(booking.vehicleLocation.long))
            
            let annot = MKPointAnnotation()

            annot.coordinate = coords
            annot.title = "Vehicle"

            self.vehicleLocation = annot
            mapView.addAnnotation(self.vehicleLocation)
        }
        
    }
    
    func updateIntermediateStops(){
        
        
        self.mapView.removeAnnotations(self.intermediateStops)

        var newStops:[MKAnnotation] = []
        if let booking = booking {
            
            var i = 1
            for stop in booking.intermediateStopLocations {
                let tempStopLoc = CLLocationCoordinate2D(latitude: CLLocationDegrees(stop.lat)  , longitude: CLLocationDegrees(stop.long))
                
                let annot = MKPointAnnotation()
                annot.coordinate = tempStopLoc
                annot.title = "Stop " + i.description
                mapView.addAnnotation(annot)
                i+=1
                newStops.append(annot)
                
            }
            self.intermediateStops = newStops
            self.mapView.addAnnotations(newStops)
        }
        
    }
    
    func updateStatus(){
        self.statusLabel.text = "Status: " + (self.booking?.status.rawValue ?? "")
        
        if self.booking?.status == Status.droppedOff{
            self.bookingClosed()
        }
    }
    
    func bookingClosed(){

        self.manager.stopConnection()

        let alert = UIAlertController(title: nil, message: "Your journey has come to an end.", preferredStyle: .alert)
        
        let dismissAction = UIAlertAction(title: "Dismiss", style: .default, handler: { [weak self] (_) in
            alert.dismiss(animated: true) { [weak self] in
                self?.dismiss(animated: true, completion: nil)
            }
        })
        
        alert.addAction(dismissAction)
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func cancelBooking(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "Verify the cancellation", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { [weak self] (_) in
            
            self?.manager.stopConnection()

            alert.dismiss(animated: true) {  [weak self] in
                self?.dismiss(animated: true, completion: nil)
            }
            
        })
        alert.addAction(cancelAction)

        let keepAction = UIAlertAction(title: "Keep it", style: .default, handler: { (_) in
            alert.dismiss(animated: true, completion: nil)
        })
        alert.addAction(keepAction)
        
        self.present(alert, animated: true, completion: nil)
    }
}

